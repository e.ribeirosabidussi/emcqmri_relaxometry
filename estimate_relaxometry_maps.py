from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging

from EMCqMRI.core.engine import build_model as core_build
from EMCqMRI.core.utilities import image_utilities
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap


def imagebrowse_slider(cube, cube2=[], axis=0, vmin_=0, vmax_=2, kwargs=[]):
    """
    Display a 3d ndarray with a slider to move along the third dimension.

    Extra keyword arguments are passed to imshow
    """
    import matplotlib.pyplot as plt
    from matplotlib.widgets import Slider, Button, RadioButtons
    from matplotlib.colors import Normalize


    colors = ['blue', 'black', 'red']
    cm = LinearSegmentedColormap.from_list('error_map', colors, N=100)
    # my_norm_scale = MidpointNormalize(midpoint=1.0, vmin=0.2, vmax=2.5)
    
    # check dim
    if not cube.ndim == 3:
        raise ValueError("cube should be an ndarray with ndim == 3")

    # generate figure
    fig = plt.figure()
    ax = plt.subplot(111)
    fig.subplots_adjust(left=0.25, bottom=0.25)

    # select first image
    s = [slice(0, 1) if i == axis else slice(None) for i in range(3)][0]
    im = cube[s].squeeze()
    if not len(cube2) == 0:
        im2 = cube2[s].squeeze()


    if kwargs == 'plot':
        # PLOT curve
        l1_l, l2_l = [], []
        for i in range(20):
            l1, = ax.plot(im[:, i], 'b--', linewidth=0.3)
            l1_l.append(l1)
            if not len(cube2) == 0:
                l2, = ax.plot(im2[:, i], 'r')
                l2_l.append(l2)
    else:
        # Display image
        l1 = ax.imshow(im, vmin=vmin_, vmax=vmax_, cmap='gray')


    axcolor = 'lightgoldenrodyellow'
    ax_slider = fig.add_axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)

    slider = Slider(ax_slider, 'Axis %i index' % axis, 0, cube.shape[axis] - 1,
                    valinit=2, valfmt='%i')

    def update(val):
        ind = int(slider.val)
        s = [slice(ind, ind + 1) if i == axis else slice(None)
                 for i in range(3)][0]
        im = cube[s].squeeze()
        if not len(cube2) == 0:
            im2 = cube2[s].squeeze()

        if kwargs == 'plot':
            for i in range(20):
                y_data1 = im[:, i]
                x_data = np.linspace(0, len(y_data1), len(y_data1));
                l1_l[i].set_data(x_data, y_data1)
                if not len(cube2) == 0:
                    y_data2 = im2[:, i]    
                    l2_l[i].set_data(x_data, y_data2)
        else:
            l1.set_data(im)

        ax.relim()
        ax.autoscale_view(True,True,True)
        fig.canvas.draw()

    slider.on_changed(update)
    plt.show()




def override_model(configurationObj):
    if not hasattr(configurationObj.args.engine, 'dataset_model'):
        if configurationObj.args.engine.datasetModel == 'custom_relaxometry':
            from custom_dataset.relaxometry import relaxometry_dataset
            configurationObj.args.engine.dataset_model = relaxometry_dataset.DatasetModel(configurationObj)

    if not hasattr(configurationObj.args.engine, 'signal_model'):
        if configurationObj.args.engine.signalModel == 'custom_looklocker':
            from custom_signal_model import looklocker
            configurationObj.args.engine.signal_model = looklocker.Looklocker(configurationObj)
        elif configurationObj.args.engine.signalModel == 'custom_fse':
            from custom_signal_model import fse
            configurationObj.args.engine.signal_model = fse.Fse(configurationObj)

    if not hasattr(configurationObj.args.engine, 'inference_model'):
        if configurationObj.args.engine.inferenceModel=='custom_rim':
            from custom_inference_model import rim
            configurationObj.args.engine.inference_model = rim.Rim(configurationObj)
        elif configurationObj.args.engine.inferenceModel=='custom_mle':
            from custom_inference_model import mle
            configurationObj.args.engine.inference_model = mle.Mle(configurationObj)
        elif configurationObj.args.engine.inferenceModel=='custom_resnet':
            from custom_inference_model import resnet
            configurationObj.args.engine.inference_model = resnet.Resnet(configurationObj)

    if not hasattr(configurationObj.args.engine, 'likelihood_model'):
        if configurationObj.args.engine.likelihoodModel=='custom_gaussian':
            from custom_likelihood import gaussian
            configurationObj.args.engine.likelihood_model = gaussian.Gaussian(configurationObj)

    configurationObj.args.engine.log_training_fun = None
    return configurationObj

if __name__=='__main__':
    configurationObj = core_build.make('testing', override_model)
    
    logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))
    logging.info('Starting inference...')

    infered_maps = configurationObj.args.engine.estimator.run(return_result = True)

    import numpy as np
    import matplotlib.pyplot as plt

    if (len(infered_maps['estimated'].shape)>4):
        last_estimate = infered_maps['estimated'][-1][0]
    else:
        last_estimate = infered_maps['estimated'][0]
        
    imagebrowse_slider(last_estimate.detach().numpy())
