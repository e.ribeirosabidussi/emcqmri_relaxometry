from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
import os
package_path = os.path.abspath(__file__ + "/../../")
sys.path.insert(0, package_path)

import logging
from EMCqMRI.core.engine import build_model as core_build


def prep_batch(batchdata, device, non_blocking=False):
    if batchdata['image'].dim() > 4:
        return batchdata['image'].to(device=device, non_blocking=non_blocking).squeeze(0), batchdata['label'].to(device=device, non_blocking=non_blocking).squeeze(0)
    else:
        return batchdata['image'].to(device=device, non_blocking=non_blocking), batchdata['label'].to(device=device, non_blocking=non_blocking)
    

def override_model(configurationObj):
    if not hasattr(configurationObj.args.engine, 'dataset_model'):
        if configurationObj.args.engine.datasetModel == 'custom_relaxometry':
            from custom_dataset.relaxometry import relaxometry_dataset
            configurationObj.args.engine.dataset_model = relaxometry_dataset.DatasetModel(configurationObj)

    if not hasattr(configurationObj.args.engine, 'signal_model'):
        if configurationObj.args.engine.signalModel == 'custom_looklocker':
            from custom_signal_model import looklocker
            configurationObj.args.engine.signal_model = looklocker.Looklocker(configurationObj)
        elif configurationObj.args.engine.signalModel == 'custom_fse':
            from custom_signal_model import fse
            configurationObj.args.engine.signal_model = fse.Fse(configurationObj)

    if not hasattr(configurationObj.args.engine, 'inference_model'):
        if configurationObj.args.engine.inferenceModel=='custom_rim':
            from custom_inference_model import rim
            configurationObj.args.engine.inference_model = rim.Rim(configurationObj)
        elif configurationObj.args.engine.inferenceModel=='custom_mle':
            from custom_inference_model import mle
            configurationObj.args.engine.inference_model = mle.Mle(configurationObj)
        elif configurationObj.args.engine.inferenceModel=='custom_resnet':
            from custom_inference_model import resnet
            configurationObj.args.engine.inference_model = resnet.Resnet(configurationObj)

    if not hasattr(configurationObj.args.engine, 'likelihood_model'):
        if configurationObj.args.engine.likelihoodModel=='custom_gaussian':
            from custom_likelihood import gaussian
            configurationObj.args.engine.likelihood_model = gaussian.Gaussian(configurationObj)

    configurationObj.args.engine.log_training_fun = None
    configurationObj.args.engine.prepare_batch = prep_batch
    return configurationObj
    

if __name__=='__main__':
    configurationObj = core_build.make('training', override_model)
    
    logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))
    
    logging.info('Starting to train the network...')
    configurationObj.args.engine.trainer.run()
