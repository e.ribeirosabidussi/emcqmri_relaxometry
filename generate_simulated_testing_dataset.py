from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import sys
import os
package_path = os.path.abspath(__file__ + "/../../")
sys.path.insert(0, package_path)

import logging
from EMCqMRI.core.engine import build_model as core_build

from EMCqMRI.core.utilities import image_utilities


def override_model(configurationObj):
    if not hasattr(configurationObj.args.engine, 'dataset_model'):
        if configurationObj.args.engine.datasetModel == 'custom_relaxometry':
            from custom_dataset.relaxometry import relaxometry_dataset
            configurationObj.args.engine.dataset_model = relaxometry_dataset.DatasetModel(configurationObj)

    if not hasattr(configurationObj.args.engine, 'signal_model'):
        if configurationObj.args.engine.signalModel == 'custom_looklocker':
            from custom_signal_model import looklocker
            configurationObj.args.engine.signal_model = looklocker.Looklocker(configurationObj)
        elif configurationObj.args.engine.signalModel == 'custom_fse':
            from custom_signal_model import fse
            configurationObj.args.engine.signal_model = fse.Fse(configurationObj)

    if not hasattr(configurationObj.args.engine, 'inference_model'):
        if configurationObj.args.engine.inferenceModel=='custom_rim':
            from custom_inference_model import rim
            configurationObj.args.engine.inference_model = rim.Rim(configurationObj)
        elif configurationObj.args.engine.inferenceModel=='custom_mle':
            from custom_inference_model import mle
            configurationObj.args.engine.inference_model = mle.Mle(configurationObj)
        elif configurationObj.args.engine.inferenceModel=='custom_resnet':
            from custom_inference_model import resnet
            configurationObj.args.engine.inference_model = resnet.Resnet(configurationObj)

    if not hasattr(configurationObj.args.engine, 'likelihood_model'):
        if configurationObj.args.engine.likelihoodModel=='custom_gaussian':
            from custom_likelihood import gaussian
            configurationObj.args.engine.likelihood_model = gaussian.Gaussian(configurationObj)

    configurationObj.args.engine.log_training_fun = None
    configurationObj.args.engine.prepare_batch = None
    return configurationObj


def generate_sim_data(configObject):

    for d, data in enumerate(configObject.args.engine.dataloader):
        signal = data['image']
        filename = data['filename']
        label = data['label']
        mask = data['mask']

        signal = signal.squeeze(0)
        label = label.squeeze(0)
        mask = mask.squeeze(0)

        
        for patch in range(len(signal)):
            data = {}
            data['weighted_series'] = signal[patch]
            data['label'] = label[patch]
            data['mask'] = signal[patch]
            print(signal[patch].shape)

            filename_ = filename[0] + 'patch_' + str(patch)

            # image_utilities.calculate_snr(signal[patch], mask[patch,-1], configObject.args.task.sigmaNoise)
            # image_utilities.saveDataPickle(data, configObject.args, filename_)



        break


if __name__=='__main__':

    configurationObj = core_build.make('training', override_model)
    # configurationObj = engine_configuration.Configuration('TESTING')
    # override_model(configurationObj)
    
    logging.info('{} model succesfully built.'.format(configurationObj.args.engine.inference_model.__name__))

    generate_sim_data(configurationObj)