from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from EMCqMRI.core.base import base_dataset
from . import dataset_utilities
import numpy as np
import os
import pickle
import torch

class DatasetModel(base_dataset.Dataset):
    def __init__(self, configObject):
        super(DatasetModel, self).__init__(configObject)
        self.__name__ = 'Relaxometry Dataset'
        self.args = configObject.args
        self.data = []
        self.idx_control = -1
        if self.args.dataset.sigmaNoise >= 0:
            self.sigma = self.args.dataset.sigmaNoise
        elif self.args.dataset.sigmaNoise < 0:
            self.sigma = None
        self.args.dataset.unitMeasure = 's'
        self.args.dataset.unit = 1

        self.set_parameter_values()
        self.set_tissue_variation()

    def prepare_anatomical_maps(self, idx):
        self.data[idx] = np.reshape(self.data[idx], (362, 434, 362))

    def set_parameter_values(self):
        # Sets parameter value based on the sequence. Here between T1 values (look locker) and T2 values (FSE)
        if self.args.engine.signalModel == 'custom_looklocker':
            tp = {"csf": 3.5, "gm": 1.4, "wm": 0.78, "fat": 0.42, 
            "muscle": 1.2, "muscle_skin": 1.23, "skull": 0.4, "vessels": 1.98, 
            "connect": 0.9, "dura": 0.9, "bone_marrow": 0.58}
            pd = {"csf": 1.0, "gm": 0.85, "wm": 0.65, "fat": 0.9,
                "muscle": 0.7, "muscle_skin": 0.7, "skull": 0.9, "vessels": 1.0,
                "connect": 0.7, "dura": 0.7, "bone_marrow": 0.8}
            
        elif self.args.engine.signalModel == 'custom_fse':
            tp = {"csf": 2, "gm": 0.11, "wm": 0.08, "fat": 0.07, 
            "muscle": 0.05, "muscle_skin": 0.05, "skull": 0.03, "vessels": 0.275, 
            "connect": 0.08, "dura": 0.07, "bone_marrow": 0.05}
            
            pd = {"csf": 1.0, "gm": 0.85, "wm": 0.65, "fat": 0.9,
                "muscle": 0.7, "muscle_skin": 0.7, "skull": 0.9, "vessels": 1.0,
                "connect": 0.7, "dura": 0.7, "bone_marrow": 0.8}

        self.tp_ = list(tp.items())
        self.pd_ = list(pd.items())

    def set_tissue_variation(self):
        if self.args.engine.signalModel == 'custom_looklocker':
            self.std_tissue = 0.3*self.args.dataset.unit
        elif self.args.engine.signalModel == 'custom_fse':
            self.std_tissue = 0.01*self.args.dataset.unit

    def get_parameter_maps(self, data_):
        qMap = np.zeros_like(data_, dtype=float)
        pdMap = np.zeros_like(data_, dtype=float)
        mask = np.zeros_like(data_, dtype=float)
        mask[data_ > 1] = 1
        epsilon_ = 0.01
        if self.args.dataset.usePatches:
            for patch in range(len(data_)):
                pngr_ = dataset_utilities.get_rand_seed(self.args.dataset.useRandomSeed)
                for unique_tissue in np.unique(data_):
                    if unique_tissue>0:
                        qMap[patch, data_[patch] == unique_tissue] = np.abs(pngr_.normal(
                                                                            self.tp_[unique_tissue-1][1]*self.args.dataset.unit, 
                                                                            self.std_tissue)
                                                                            )+epsilon_
                        
                        pdMap[patch, data_[patch] == unique_tissue] = np.abs(pngr_.normal(
                                                                            self.pd_[unique_tissue-1][1], 
                                                                            0.3)
                                                                            )+epsilon_
        else:
            for unique_tissue in np.unique(data_):
                if unique_tissue > 0 :    
                    qMap[data_ == unique_tissue] = np.abs(pngr_.normal(
                                                        self.tp_[unique_tissue-1][1]*self.args.dataset.unit, 
                                                        self.std_tissue)
                                                        )+epsilon_

                    pdMap[data_ == unique_tissue] = np.abs(pngr_.normal(
                                                        self.pd_[unique_tissue-1][1],
                                                        0.3)
                                                        )+epsilon_
        
        qMap = np.stack(qMap)
        pdMap = np.stack(pdMap)

        if self.args.engine.signalModel == 'custom_looklocker':
            bMap = []
            for pi, p_map in enumerate(pdMap):
                pngr_ = dataset_utilities.get_rand_seed(self.args.dataset.useRandomSeed)
                abs_bMap = np.abs(pngr_.normal(2.0, 0.2, np.shape(p_map)))
                half_normal_bMap_ = (2 - np.abs(abs_bMap - 2))*mask[pi]
                bMap.append(half_normal_bMap_)
            bMap = np.stack(bMap)
            kappa = [pdMap, bMap, qMap]
        elif self.args.engine.signalModel == 'custom_fse':
            kappa = [pdMap, qMap]
        
        return kappa

    def generate_simulated_training_label(self, idx):
        data_ = self.data[idx]
        pngr_ = dataset_utilities.get_rand_seed(self.args.dataset.useRandomSeed)
        self.args.dataset.pngr = pngr_
        if self.args.dataset.usePatches:
            patch_extractor = dataset_utilities.ExtractPatch(self.args)
            data_ = patch_extractor.get_patch(data_)

        # Only for 2D
        slice_index = int(pngr_.uniform(10, int(data_.shape[-1])-10))
        data_ = data_[..., slice_index]

        kappa = self.get_parameter_maps(data_)
        kappa = dataset_utilities.apply_gt_noise(kappa, pngr_, self.args)
        kappa = dataset_utilities.smooth_maps(kappa, self.args)

        if self.args.dataset.simulateArtefacts:
            kappa = dataset_utilities.add_artefacts(kappa)

        self.training_label = torch.from_numpy(np.abs(kappa)).type(torch.FloatTensor).to(self.args.engine.device)

        self.mask = torch.ones_like(self.training_label[0])#np.expand_dims(mask, 1).repeat(self.training_label.size()[1], 1)
        # self.mask = torch.from_numpy(mask).type(torch.FloatTensor).to(self.args.engine.device)

    def generate_simulated_training_signal(self):
        weightedSeries = []
        for label_patch in self.training_label:
            weighted_images = self.args.engine.signal_model.forward(label_patch)
            weighted_images_ = weighted_images.type(torch.FloatTensor).to(self.args.engine.device)

            # Set sigma for acquisition noise
            if not self.sigma:
                s = torch.distributions.log_normal.LogNormal(0.0, 1)
                self.sigma = (s.sample()/60)*self.args.dataset.unit

            weighted_images_noisy = self.args.engine.likelihood_model.apply_noise(weighted_images, self.sigma)
            weightedSeries.append(weighted_images_noisy)
            
        self.training_signal = torch.stack(weightedSeries)
        # print("signal shape: ", self.training_label.shape)

    def get_existing_data(self, idx):
        if not self.args.dataset.preLoadData:
            data_ = self.data[idx - self.idx_control]
        else:
            data_ = self.data[idx]
        
        self.training_signal = torch.Tensor(data_['weighted_series']).type(torch.FloatTensor)
        self.training_signal = self.training_signal/torch.max(self.training_signal)

        if 'label' in data_:
            self.training_label = torch.Tensor(data_['label']).type(torch.FloatTensor)
        else:
            self.training_label = 0

        if 'mask' in data_:
            self.mask = torch.Tensor(data_['mask']).type(torch.FloatTensor)
        else:
            self.mask = []

        if self.training_signal.dim() > 3:
            self.training_signal = self.training_signal[...,8]

    def get_label(self, idx):
        if self.args.dataset.useSimulatedData:
            self.load_folder()
            self.prepare_anatomical_maps(idx)
            self.generate_simulated_training_label(idx)
        else:
            self.load_file(idx)
            self.get_existing_data(idx)
        if self.args.dataset.numberOfPatches > 1:
            self.training_label = self.training_label.squeeze(0)
        return self.training_label, self.mask

    def get_signal(self, *local_args):
        if self.args.dataset.useSimulatedData:
            self.generate_simulated_training_signal()
        if self.args.dataset.numberOfPatches > 1:
            self.training_signal = self.training_signal.squeeze(0)
        return self.training_signal
