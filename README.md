# EMCqMRI_relaxometry


This repository contains the open-source code from the paper:

"Recurrent Inference Machines as inverse problem solvers for MR relaxometry", E.R. Sabidussi et al. - https://doi.org/10.1016/j.media.2021.102220

*******************************

This project uses the EMCqMRI version 0.2.1, which is not the latest version of the package.

# **Installing the EMCqMRI**

## Preparing the development enviroment

- Create a virtual environment for the project: `python3 -m venv venv_example_project`

- Activate the virtual environment: `. venv_example_project/bin/activate`

- Upgrade pip and install wheel: `pip3 install --upgrade pip wheel` (this will make sure you have no errors and warnings during installations with pip)

## Installing PyTorch

- MacOS without CUDA support: `pip3 install torch torchvision`

- MacOS with CUDA support: follow instructions at this URL: https://github.com/pytorch/pytorch#from-source

- Linux with CUDA support: `pip3 install torch torchvision`

- Windows with CUDA support: `pip3 install torch==1.7.1 torchvision==0.8.2 -f https://download.pytorch.org/whl/torch_stable.html`

## Installing the EMCqMRI package with PIP
Finally, run:

```shell
pip3 install -i https://test.pypi.org/simple/ --extra-index-url https://pypi.python.org/simple/ --upgrade EMCqMRI==0.2.1
```

For the more information on the EMCqMRI package, please refer to: https://gitlab.com/e.ribeirosabidussi/emcqmri.


## Cloning the EMCqMRI_relaxometry
Now, you can clone this repository and run the commands below (from within the newly cloned folder)

# Usage 
To estimate T1 and T2 relaxometry maps with each method:

## T1 mapping:
### Recurrent Inference Machines
`
python3 estimate_relaxometry_maps.py -configurationFile custom_configuration/rim/testing/t1/engine_conf.txt
`
### ResNet
`
python3 estimate_relaxometry_maps.py -configurationFile custom_configuration/resnet/t1/engine_conf.txt
`

### MLE
`
python3 estimate_relaxometry_maps.py -configurationFile custom_configuration/mle/t1/engine_conf.txt
`


## T2 mapping:
### Recurrent Inference Machines
`
python3 estimate_relaxometry_maps.py -configurationFile custom_configuration/rim/testing/t2/engine_conf.txt
`

### ResNet
`
python3 estimate_relaxometry_maps.py -configurationFile custom_configuration/resnet/t2/engine_conf.txt
`

### MLE
`
python3 estimate_relaxometry_maps.py -configurationFile custom_configuration/mle/t2/engine_conf.txt
`


# Training the Relaxometry RIM
In the example set here, we use simulated T1/T2 weighted images to train the network.

### T1 mapping
`
python3 train_model.py -configurationFile custom_configuration/rim/training/t1/engine_conf.txt
`

### T2 mapping
`
python3 train_model.py -configurationFile custom_configuration/rim/training/t2/engine_conf.txt
`
