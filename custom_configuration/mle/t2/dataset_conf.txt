{ "dataset-configuration": {
    "task":{
        "signalModel": "fse",
        "likelihoodModel": "gaussian"
    },

    "dataset":{
        "preLoadData": true,
        "usePatches": false,
        "patchSize": 80,
        "useSimulatedData": false,
        "sigmaNoise": 0.002,
        "simulateArtefacts": false,
        "useRandomSeed": true,
        "tau": [10, 20, 40, 80, 160, 320],
        "regularizationWeights": [0.001, 0.001]
    }
}
}
