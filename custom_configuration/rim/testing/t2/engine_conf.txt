{ "Configuration-file": {

    "module-configuration":{
        "configInference": "custom_configuration/rim/testing/t2/rim_conf.txt",
        "configDataset": "custom_configuration/rim/testing/t2/dataset_conf.txt"
    },


    "model-build":{
        "inferenceModel": "custom_rim",
        "signalModel": "custom_fse",
        "likelihoodModel": "custom_gaussian",
        "datasetModel": "custom_relaxometry",
        "lossFunction": "MSE",
        "trainerModule": "emcqmri",
        "estimatorModule": "emcqmri",
        "optimizer": "ADAM",
        "useCUDA": false,
        "runBenchmark": false,
        "allowCMDoverride": false,
        "displayConfigurations": true
    },

    "datasets":{
        "trainingDataPath": "example_data/training/anatomical_models",
        "runValidation": false,
        "validationDataPath": "",
        "testingDataPath": "example_data/testing/t2_invivo",
        "fileExtension": "pkl",
        "preLoadData": false,
        "shuffleData": false
    },

    "training-engine":{
        "mode": "testing",
        "learningRate": 0.005,
        "batchSize": 1,
        "epochs": 1,
        "numWorkers": 0,
        "loadCheckpoint": true,
        "loadCheckpointPath": "trained_models/RIM/relaxometry/T2/RIM_T2_6T_36C_6TE_ReLu_24Batch_fixedNoise_T2init_299.pth",
        "saveResults": false,
        "saveResultsPath": "",
        "saveCheckpoint": false,
        "saveCheckpointPath": "",
        "sulfixCheckpoint": "",
        "writeTensorboard": false,
        "experimentName": "",
        "makeMemProfile": false
    }
    }
} 